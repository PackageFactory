require 'Settings'

class Package<Settings

	attr_reader :name, :version, :release, :license, :homepage, :source, :depends, :rdepends, :summary, :description, :changes

	def initialize()
		super
		@name        = ''
		@version     = ''
		@release     = '0'
		@license     = ''
		@homepage    = ''
		@depends     = []
		@rdepends    = []
		@summary     = ''
		@description = ''
		@changes     = []
		@source      = ''
		@buildroot   = ''
	end

	def buildroot()
		if @buildroot != nil and @buildroot != ''
			return @buildroot
		else
			return '/var/tmp/%s-%s-%s-build' % [@name, @version, @release]
		end
	end
	
	def source()
		if @source != nil and @buildroot != ''
			return @source
		else
			return '%s-%s.tar.bz2' % [ @name, @version ]
		end
	end

	def prepare()
		return '%setup -q'
	end

	def build()
		return ''
	end

	def install()
		return ''
	end

	def clean()
		return 'rm -rf ' + buildroot()
	end

	def pre()
		return ''
	end

	def post()
		return ''
	end

	def preun()
		return ''
	end

	def postun()
		return ''
	end

	def files()
		return ''
	end

end
