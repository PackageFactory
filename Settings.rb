class Settings

	attr_reader :arch, :jobs, :host, :build, :target, :optflags, :cflags, :cxxflags, :fflags
	attr_reader :prefix, :sysconfdir, :localstatedir, :bindir, :sbindir, :gamesbindir, :includedir, :datadir, :gamesdatadir, :mandir, :infodir, :lib, :libdir, :libexecdir

	def initialize()

		@arch          = 'i586'
		@jobs          = 1

		@host          = 'x86_64-suse-linux-gnu'
		@build         = 'x86_64-suse-linux-gnu'
		@target        = 'x86_64-suse-linux'

		@optflags      = '-O2 -g -fmessage-length=0 -D_FORTIFY_SOURCE=2 -fstack-protector -funwind-tables -fasynchronous-unwind-tables'

		@cflags        = @optflags
		@cxxflags      = @optflags
		@fflags        = @optflags

		@prefix        = '/usr'
		@sysconfdir    = '/etc'
		@localstatedir = '/var'
		@bindir        = @prefix + '/bin'
		@sbindir       = @prefix + '/sbin'
		@gamesbindir   = @prefix + '/games'
		@includedir    = @prefix + '/include'
		@datadir       = @prefix + '/share'
		@gamesdatadir  = @datadir + '/games'
		@mandir        = @datadir + '/man'
		@infodir       = @datadir + '/info'
		@lib           = 'lib'
		@libdir        = @prefix + '/' + @lib
		@libexecdir    = @prefix + '/' + @lib

	end

end
