require 'Package'

class Autotools<Package

	def initalize()
		super
	end

	def build()
		return "export CFLAGS='%s'\n" % @cflags +
		"export CXXFLAGS='%s'\n" % @cxxflags +
		"export FFLAGS='%s'\n" % @fflags+
		"./configure --host=%s --build=%s --target=%s \\\n" % [ @host, @build, @target ] +
		"--program-prefix= \\\n" +
		"--prefix=%s \\\n" % @prefix +
		"--exec-prefix=%s \\\n" % @prefix +
		"--bindir=%s \\\n" % @bindir +
		"--sbindir=%s \\\n" % @sbindir +
		"--sysconfdir=%s \\\n" % @sysconfdir +
		"--datadir=%s \ \\\n" % @datadir +
		"--includedir=%s \ \\\n" % @includedir +
		"--libdir=%s \ \\\n" % @libdir +
		"--libexecdir=%s \ \\\n" % @libexecdir+
		"--localstatedir=%s \ \\\n" % @localstatedir +
		"--mandir=%s \ \\\n" % @mandir +
		"--infodir=%s\n" % @infodir +
		"make -j %s\n" % @jobs
	end

	def install()
		return "make DESTDIR=%s install" % buildroot()
	end

end
