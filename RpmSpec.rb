require 'Package'

class RpmSpec

	@spec = ''

	def initialize(pkg)
		if not pkg.kind_of?(Package)
			fail 'RpmSpec: Argument is not a package'
		end
		@spec = createspec(pkg)
	end

	def createspec(pkg)
		s = "Name: %s\n" % pkg.name +
		"Version: %s\n" % pkg.version +
		"Release: %s\n" % pkg.release +
		"Summary: %s\n" % pkg.summary +
		"License: %s\n" % pkg.license +
		"Url: %s\n" % pkg.homepage +
		"Source: %s\n" % pkg.source +
		"BuildRoot: %s\n" % pkg.buildroot +
		"BuildRequires: %s\n" % pkg.depends.join(' ') +
		"Requires: %s\n\n" % pkg.rdepends.join(' ') +
		"%%description\n%s\n\n" % pkg.description +
		"%%prep\n%s\n\n" % pkg.prepare() +
		"%%build\n%s\n\n" % pkg.build() +
		"%%install\n%s\n\n" % pkg.install() +
		"%%clean\n%s\n\n" % pkg.clean()

		t = pkg.pre()
		s += "%%pre\n%s\n\n" % t if t != nil and t != ''

		t = pkg.post()
		s += "%%postun\n%s\n\n" % t if t != nil and t != ''

		t = pkg.preun()
		s += "%%preun\n%s\n\n" % t if t != nil and t != ''

		t = pkg.postun()
		s += "%%postun\n%s\n\n" % t if t != nil and t != ''

		s += "%%files\n%s\n\n" % pkg.files()

		s += "%%changelog\n%s\n\n" % pkg.changes().join()

		return s
	end

	def print()
		puts @spec
	end

end
