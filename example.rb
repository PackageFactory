require 'Autotools'

class Example<Autotools

	def initialize()
		super
		@name        = 'example'
		@version     = '0.1.2'
		@license     = 'GPLv2'
		@homepage    = 'http://www.example.com'
		@depends     = [ 'gtk2-devel', 'update-desktop-files' ]
		@rdepends    = [ 'tango-icon-theme' ]
		@summary     = 'An example application'
		@description = 'This is an example application'
	end

end
